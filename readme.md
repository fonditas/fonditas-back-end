# Learn about laravel

```
# 1. https://www.youtube.com/watch?v=LQdGfYYOlLk&t=100s
```

# Installation process
```
# 1. Clone the repo
$: git clone https://bitbucket.org/fonditas/fonditas-back-end
# 2. Move to the recently created folder
$: cd folder
# 3. Make your own environment settings
$: cp .env.example .env
# 4. Set your database settings don't forget to create your database
$: vi .env
# 5. Install and update composer
$: composer install
$: composer update
# 6. Generate your artisan key
$: php artisan key:generate
# 7. Make the migrations
$: php artisan migrate
# 8. Make the migrations
$: php artisan db:seed
```

# Development process highlights
- Never never ever develop on `master`, always create a new branch.
- Don't forget to check out your working branch.
- All code must be written in English.
- All models must be written in English.
