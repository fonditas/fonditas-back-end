<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Restaurant;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'email',
        'password',
        'latitude',
        'longitude',
        'photo_url'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'password', 'remember_token',
    ];

    /**
     * Get the restaurant record associated with the user.
     */
    public function restaurant()
    {
        return $this->hasOne('App\Models\Restaurant')->with('products');
    }

    /**
     * Get the products for the restaurant.
     */
    public function orders()
    {
        return $this->hasMany('App\Models\Order')->with('restaurant');
    }

    /**
     * Get the favourites.
     */
    public function favourites()
    {
        return $this->hasMany('App\Models\Favourite');
    }

}
