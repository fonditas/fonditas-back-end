<?php
namespace App\Libraries;

use Illuminate\Contracts\Support\Arrayable;
use JsonSerializable;

class ErrorResponse implements Arrayable, JsonSerializable {
    
    /**
     * Error container
     * @var array
     */
    private $error;

    function __construct($code, $message) {
        $this->error = array('code' => $code, 'message' => $message);
    }

    /**
     * Converts the object into an array
     * @return array
     */
    function toArray() {
        return $this->error;
    }

    /**
     * Objects implementing JsonSerializable can customize their
     * JSON representation when encoded with json_encode().
     * @return mixed
     */
    function jsonSerialize() {
        return $this->error;
    }

    /**
     * Creates the error object
     * @param  int $code http code
     * @param  string $message message
     * @return App\Libraries\ErrorResponse created object
     */
    static function create($code, $message) {
        return new ErrorResponse($code, $message);
    }

}
