<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    protected $casts = [
        'active' => 'boolean',
        'sold_out' => 'boolean',
        'deleted' => 'boolean'
    ];

    protected $appends = array('quantity');

    public function getQuantityAttribute() {
        return 0;
    }

    /**
     * Get the restaurant that owns the product.
     */
    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }

    /**
     * The order products that belongs to this record.
     */
    public function users()
    {
        return $this->hasMany('App\Models\OrderProduct');
    }

}
