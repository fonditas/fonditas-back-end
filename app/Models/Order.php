<?php

namespace App\Models;
use App\Models\OrderProduct;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    protected $appends = array('products');

    public function getProductsAttribute(){
        return $this->orderProducts()->get();
    }

    /**
     * Get the restaurant that owns the product.
     */
    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }

    /**
     * Get the user that owns the order.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the products in the order.
     */
    public function orderProducts()
    {
        return $this->hasMany('App\Models\OrderProduct')->with('product');
    }

}
