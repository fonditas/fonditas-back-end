<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $table = 'restaurants';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    protected $casts = [
        'open' => 'boolean',
    ];

    protected $appends = array('dayMenu');

    public function getDayMenuAttribute() {
        return array(
            "starters" => array(),
            "midDish" => array(),
            "mainDish" => array(),
            "desserts" => array(),
            "drinks" => array(),
        );
    }

    /**
     * Get the user that owns the restaurant.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the products for the restaurant.
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product')->where('deleted', '=', false);
    }

    /**
     * Get the products for the restaurant.
     */
    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    /**
     * Get the favourites.
     */
    public function favourites()
    {
        return $this->hasMany('App\Models\Favourite');
    }

}
