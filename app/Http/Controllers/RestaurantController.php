<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Restaurant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Libraries\ErrorResponse;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;

class RestaurantController extends Controller
{
    /**
     * Store a new restaurant.
     * @return Response
     */
    public function store() {

        $data = [];
        $code = Response::HTTP_CREATED;

        try {

            $token = JWTAuth::getToken();
            if ( !$token ) {

              return response()->json(['error' => 'invalid credentials'], 401);

            } else {

              $user = JWTAuth::toUser( $token );
              $restaurant = new Restaurant();
              $restaurant->user_id = $user->id;
              $restaurant->name = request()->input('restaurant.name');
              $restaurant->description = request()->input('restaurant.description');
              $restaurant->address = request()->input('restaurant.address');
              $restaurant->latitude = request()->input('restaurant.latitude');
              $restaurant->longitude = request()->input('restaurant.longitude');
              $restaurant->photo_url = request()->input('restaurant.photo_url');
              $restaurant->save();
              $data['restaurant'] = $restaurant;

            }

        } catch ( \Illuminate\Database\QueryException $e ) {
            // There was an error
            $code = Response::HTTP_UNPROCESSABLE_ENTITY;
            $data = [];
            $data['error'] = ErrorResponse::create($code, $e->getMessage());
        }
        return response()->json($data, $code);
    }

    /**
    * List restaurants.
    * @return Response
    */
    public function index() {
        $token = JWTAuth::getToken();
        if ( !$token ) {

          return response()->json(['error' => 'invalid credentials'], 401);

        } else {

            $data['restaurants'] = Restaurant::with(['products' => function ($query) {
                $query->where( 'active', 1 )->orderBy( 'category', 'asc' );
            }])->with('user')->get();
            return response()->json( $data );

        }
    }

}
