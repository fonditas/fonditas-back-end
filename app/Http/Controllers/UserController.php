<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use App\User;
use App\Models\Order;
use App\Models\Favourite;
use App\Models\Restaurant;
use Illuminate\Http\Response;
use App\Libraries\ErrorResponse;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;

class UserController extends Controller{

    public function myOrders() {
        $token = JWTAuth::getToken();
        if ( !$token ) {

          return response()->json(['error' => 'invalid credentials'], 401);

        } else {

            $user = JWTAuth::toUser( $token );
            $data['orders'] = $user->orders()->get();
            return response()->json( $data );

        }
    }

    public function myRestaurant() {

        $token = JWTAuth::getToken();

        if ( !$token ) {
            return response()->json(['error' => 'invalid credentials'], 401);
        } else {

            $user = JWTAuth::toUser( $token );
            $restaurant = $user->restaurant;
            if ( is_null( $restaurant ) ){
              return response()->json(['error' => 'invalid operation'], 406);
            }
            $data['restaurant'] = $user->restaurant;
            return response()->json( $data );

        }
    }

    public function openCloseRestaurant() {
        $token = JWTAuth::getToken();
        if ( !$token ) {
            return response()->json(['error' => 'invalid credentials'], 401);
        } else {

            $user = JWTAuth::toUser( $token );
            $restaurant = $user->restaurant;
            if ( is_null( $restaurant ) ){
              return response()->json(['error' => 'invalid operation'], 406);
            }
            $restaurant->open = !$restaurant->open;
            $restaurant->save();
            if ( !$restaurant->open ) {
                foreach ($restaurant->products as $product) {
                    $product->active = 0;
                    $product->sold_out = 0;
                    $product->save();
                }
            }
            $data['restaurant'] = $user->restaurant;
            return response()->json( $data, 200 );
        }
    }

    public function addFavourite() {
        $data = [];
        $code = 200;
        try {

            $token = JWTAuth::getToken();
            if ( !$token ) {

                return response()->json(['error' => 'invalid credentials'], 401);

            } else {
                $data['favourite'] = null;
                $user = JWTAuth::toUser( $token );
                $restaurant = $user->restaurant;
                $restaurant = Restaurant::findOrFail( request()->input('restaurant.id') );
                $previousFavourite = Favourite::where('restaurant_id', '=', $restaurant->id)->where('user_id', '=', $user->id)->first();
                $data['favourite'] = $previousFavourite;
                if ( !is_null( $previousFavourite ) ) {
                    $data['favourite'] = $previousFavourite;
                } else {
                    $favourite = new Favourite();
                    $favourite->user_id = $user->id;
                    $favourite->restaurant_id = $restaurant->id;
                    $favourite->save();
                    $data['favourite'] = $favourite;
                }
                $restaurants = array();
                $favourites = $user->favourites()->get();
                foreach ( $favourites as $favourite ) {
                    array_push( $restaurants, $favourite->restaurant_id );
                }
                $data['favourites'] = $restaurants;
            }

        } catch ( \Illuminate\Database\QueryException $e ) {
            // There was an error
            $code = Response::HTTP_UNPROCESSABLE_ENTITY;
            $data = [];
            $data['error'] = ErrorResponse::create($code, $e->getMessage());
        } catch( ModelNotFoundException $e ) {
            return response()->json(['error' => 'invalid operation'], 406);
        }
        return response()->json($data, $code);
    }

    public function removeFavourite() {
        $data = [];
        $code = 200;
        try {

            $token = JWTAuth::getToken();
            if ( !$token ) {

                return response()->json(['error' => 'invalid credentials'], 401);

            } else {
                $user = JWTAuth::toUser( $token );
                $restaurant = $user->restaurant;
                $restaurant = Restaurant::findOrFail( request()->input('restaurant.id') );
                $previousFavourite = Favourite::where('restaurant_id', '=', $restaurant->id)->where('user_id', '=', $user->id)->firstOrFail();
                $previousFavourite->delete();
                $data['deleted'] = 'true';
                $restaurants = array();
                $favourites = $user->favourites()->get();
                foreach ( $favourites as $favourite ) {
                    $r = Restaurant::with('products', 'user')->find( $favourite->restaurant_id );
                    array_push( $restaurants,  $r );
                }
                $data['restaurants'] = $restaurants;
            }

        } catch ( \Illuminate\Database\QueryException $e ) {
            // There was an error
            $code = Response::HTTP_UNPROCESSABLE_ENTITY;
            $data = [];
            $data['error'] = ErrorResponse::create($code, $e->getMessage());
        } catch( ModelNotFoundException $e ) {
            return response()->json(['error' => 'invalid operation'], 406);
        }
        return response()->json($data, $code);
    }

    public function myFavourites() {
        $token = JWTAuth::getToken();
        if ( !$token ) {

          return response()->json(['error' => 'invalid credentials'], 401);

        } else {

            $user = JWTAuth::toUser( $token );
            $restaurants = array();
            $favourites = $user->favourites()->get();
            foreach ( $favourites as $favourite ) {
                $r = Restaurant::with('products', 'user')->find( $favourite->restaurant_id );
                array_push( $restaurants,  $r );
            }
            $data['restaurants'] = $restaurants;
            return response()->json( $data );

        }
    }

}
