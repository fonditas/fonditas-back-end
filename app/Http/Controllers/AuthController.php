<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use App\User;
use App\Models\Restaurant;
use Illuminate\Http\Response;
use App\Libraries\ErrorResponse;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;

class AuthController extends Controller{

  public function authenticate(){

    $credentials = request()->only('email','password');
    $code = Response::HTTP_OK;
    $data = [];

    try {
      $token = JWTAuth::attempt($credentials);
      if ( !$token ) {
        return response()->json(['error' => 'invalid credentials'], 401);
      }
    } catch(JWTException $e) {
      return response()->json(['error' => 'something went wrong'], 500);
    }

    $data['token'] = $token;
    $user = JWTAuth::toUser( $token );
    if ( !is_null( $user->restaurant ) ){
        $data['owner'] = true;
    } else {
        $data['owner'] = false;
    }
    return response()->json($data, $code);

  }

  public function store(){

    $data = [];
    $code = Response::HTTP_CREATED;

    try {

      $email = request()->input('user.email');
      $name = request()->input('user.name');
      $last_name = request()->input('user.last_name');
      $password = request()->input('user.password');
      $image = (request()->has('user.photoBase64'))? request()->input('user.photoBase64') : null;


      $user = User::create([
        'name' => $name,
        'last_name' => $last_name,
        'email' => $email,
        'password' => bcrypt($password),
        'photo_url' => $image
      ]);

      $token = JWTAuth::fromUser($user);
      $data['token'] = $token;

    } catch(\Illuminate\Database\QueryException $e){

      return response()->json(['error' => 'the email is already registered'], 422);

    }

    return response()->json($data, $code);

  }

  public function storeWithRestaurant(){

    $data = [];
    $code = Response::HTTP_CREATED;

    try {

      $email = request()->input('user.email');
      $name = request()->input('user.name');
      $last_name = request()->input('user.last_name');
      $password = request()->input('user.password');
      $image = (request()->has('user.photoBase64'))? request()->input('user.photoBase64') : null;


      $user = User::create([
        'name' => $name,
        'last_name' => $last_name,
        'email' => $email,
        'password' => bcrypt($password),
        'photo_url' => $image
      ]);

      $token = JWTAuth::fromUser($user);
      $data['token'] = $token;

      $restaurant = new Restaurant();
      $restaurant->user_id = $user->id;
      $restaurant->name = request()->input('restaurant.name');
      $restaurant->description = request()->input('restaurant.description');
      $restaurant->address = request()->input('restaurant.address');
      $restaurant->latitude = request()->input('restaurant.latitude');
      $restaurant->longitude = request()->input('restaurant.longitude');
      $restaurant->photo_url = request()->input('restaurant.photo_url');
      $restaurant->save();
      $data['restaurant'] = $restaurant;

    } catch(\Illuminate\Database\QueryException $e){

        $code = Response::HTTP_UNPROCESSABLE_ENTITY;
        $data = [];
        $data['error'] = ErrorResponse::create($code, $e->getMessage());

    }

    return response()->json($data, $code);

  }

  public function checkAvailability(){

    $email = request()->input('email');
    $data = [];
    $code = Response::HTTP_OK;

    try {
        $result = User::where('email', $email)->firstOrFail();
        $data['available'] = false;
    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        $data['available'] = true;
    } catch (\Illuminate\Database\QueryException $e) {
        return response()->json(['error' => 'there was an error on your request'], 400);
    }
    return response()->json($data, $code);

  }

}
