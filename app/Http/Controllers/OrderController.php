<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Restaurant;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Libraries\ErrorResponse;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;

class OrderController extends Controller
{
    /**
     * Store a new order.
     * @return Response
     */
    public function store() {

        $data = [];
        $code = Response::HTTP_CREATED;

        try {

            $token = JWTAuth::getToken();
            if ( !$token ) {

              return response()->json(['error' => 'invalid credentials'], 401);

            } else {

              $user = JWTAuth::toUser( $token );
              $restaurant = Restaurant::find( request()->input('order.restaurantId') );

              if ( is_null( $restaurant ) ){
                return response()->json(['error' => 'invalid operation'], 406);
              }

              $order = new Order();
              $order->total = request()->input('order.total');
              $order->paypal_id = request()->input('order.paypalId');
              $order->hour_of_arrive = request()->input('order.hourOfArrive');
              $order->restaurant_id = $restaurant->id;
              $order->user_id = $user->id;
              $order->save();

              $products = request()->input('order.products');

              foreach ($products as $product) {
                $orderProduct = new OrderProduct();
                $orderProduct->quantity = $product['quantity'];
                $orderProduct->total = $product['quantity'] * $product['price'];
                $orderProduct->order_id = $order->id;
                $orderProduct->product_id = $product['id'];
                $orderProduct->save();
              }

              $data['order'] = Order::find($order->id);

            }

        } catch ( \Illuminate\Database\QueryException $e ) {
            // There was an error
            $code = Response::HTTP_UNPROCESSABLE_ENTITY;
            $data = [];
            $data['error'] = ErrorResponse::create($code, $e->getMessage());
        }
        return response()->json($data, $code);
    }


}
