<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Restaurant;
use App\Models\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Libraries\ErrorResponse;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;

class ProductController extends Controller
{
    /**
     * Store a new product for a restaurant.
     * @return Response
     */
    public function store() {

        $data = [];
        $code = Response::HTTP_CREATED;

        try {

            $token = JWTAuth::getToken();
            if ( !$token ) {

              return response()->json(['error' => 'invalid credentials'], 401);

            } else {

              $user = JWTAuth::toUser( $token );
              $restaurant = $user->restaurant;

              if ( is_null( $restaurant ) ){
                return response()->json(['error' => 'invalid operation'], 406);
              }

              $product = new Product();
              $product->name = request()->input('product.name');
              $product->description = request()->input('product.description');
              $product->category = request()->input('product.category');
              $product->price = request()->input('product.price');
              $product->restaurant_id = $restaurant->id;
              if( request()->has('product.photo_url') ){
                $product->photo_url =  request()->input('product.photo_url');
              }
              $product->save();
              $data['product'] = $product;

            }

        } catch ( \Illuminate\Database\QueryException $e ) {
            // There was an error
            $code = Response::HTTP_UNPROCESSABLE_ENTITY;
            $data = [];
            $data['error'] = ErrorResponse::create($code, $e->getMessage());
        }
        return response()->json($data, $code);
    }

    /**
    * Modifies the value of a product.
    * @return Response
    */
    public function update() {

        $data = [];
        $code = 200;

        try {

            $token = JWTAuth::getToken();
            if ( !$token ) {

              return response()->json(['error' => 'invalid credentials'], 401);

            } else {

              $user = JWTAuth::toUser( $token );
              $restaurant = $user->restaurant;

              if ( is_null( $restaurant ) ){
                return response()->json(['error' => 'invalid operation'], 406);
              }

              $product = Product::findOrFail( request()->input('product.id') );
              if ( $product->restaurant_id != $restaurant->id ){
                return response()->json(['error' => 'invalid operation'], 406);
              }
              $product->name = request()->input('product.name');
              $product->description = request()->input('product.description');
              $product->category = request()->input('product.category');
              $product->price = request()->input('product.price');
              if( request()->has('product.photo_url') ){
                $product->photo_url =  request()->input('product.photo_url');
              }
              $product->save();
              $data['product'] = $product;
            }

        } catch ( \Illuminate\Database\QueryException $e ) {
            // There was an error
            $code = Response::HTTP_UNPROCESSABLE_ENTITY;
            $data = [];
            $data['error'] = ErrorResponse::create($code, $e->getMessage());
        } catch( ModelNotFoundException $e ) {
            return response()->json(['error' => 'invalid operation'], 406);
        }
        return response()->json($data, $code);
    }

    /**
    * Make a logical delete of a product.
    * @return Response
    */
    public function delete() {
        $data = [];
        $code = 200;
        try {

            $token = JWTAuth::getToken();
            if ( !$token ) {

              return response()->json(['error' => 'invalid credentials'], 401);

            } else {

              $user = JWTAuth::toUser( $token );
              $restaurant = $user->restaurant;

              if ( is_null( $restaurant ) ){
                return response()->json(['error' => 'invalid operation'], 406);
              }

              $product = Product::findOrFail( request()->input('product.id') );
              if ( $product->restaurant_id != $restaurant->id ){
                return response()->json(['error' => 'invalid operation'], 406);
              }
              $product->deleted = true;
              $product->save();
              $data['product'] = $product;
            }

        } catch ( \Illuminate\Database\QueryException $e ) {
            // There was an error
            $code = Response::HTTP_UNPROCESSABLE_ENTITY;
            $data = [];
            $data['error'] = ErrorResponse::create($code, $e->getMessage());
        } catch( ModelNotFoundException $e ) {
            return response()->json(['error' => 'invalid operation'], 406);
        }
        return response()->json($data, $code);
    }


    /**
     * Update the status of a product.
     * @return Response
     */
    public function activeProducts() {
        $data = [];
        $token = JWTAuth::getToken();
        if ( !$token ) {
            return response()->json(['error' => 'invalid credentials'], 401);
        } else {

            $user = JWTAuth::toUser( $token );
            $restaurant = $user->restaurant;
            if ( is_null( $restaurant ) ){
              return response()->json(['error' => 'invalid operation'], 406);
            }

            $activeProducts = $restaurant->products;
            foreach ( $activeProducts as $p ) {
                $p->active = false;
                $p->sold_out = false;
                $p->save();
            }

            $products = request()->input('products');
            $soldOuts = request()->input('soldOuts');

            $productsResponse = array();

            foreach ( $products as $p ) {
                $product = Product::find($p);
                if ( !is_null( $product ) ) {
                    if ( $product->restaurant_id == $restaurant->id ) {
                        $product->active = true;
                        $product->save();
                        array_push($productsResponse, $product);
                    }
                }
            }

            $productsOutResponse = array();
            foreach ( $soldOuts as $p ) {
                $product = Product::find($p);
                if ( !is_null( $product ) ) {
                    if ( $product->restaurant_id == $restaurant->id ) {
                        if ( $restaurant->open && $product->active ) {
                            $product->sold_out = true;
                            $product->save();
                            array_push($productsOutResponse, $product);
                        }
                    }
                }
            }

            $data['products'] = $productsResponse;
            $data['soldOuts'] = $productsOutResponse;
            return response()->json( $data, 200 );
        }
    }



}
