<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', 'AuthController@store');

Route::post('/register-with-restaurant', 'AuthController@storeWithRestaurant');

Route::post('/authenticate', 'AuthController@authenticate');

Route::post('/checkAvailability', 'AuthController@checkAvailability');

Route::group(['middleware' => ['jwt.refresh']], function(){

    Route::get('/user', function (Request $request) {
        return ['name' => 'Jacobo'];
    });
    /*
    |--------------------------------------------------------------------------
    | Rutas de fonditas
    |--------------------------------------------------------------------------
    */
    Route::get('/restaurants', 'RestaurantController@index')->name('restaurants.index');
    Route::post('/restaurants', 'RestaurantController@store')->name('restaurants.store');

    /*
    |--------------------------------------------------------------------------
    | Rutas de productos de fonditas
    |--------------------------------------------------------------------------
    */
    //Route::get('/products', 'ProductController@index')->name('products.index');
    Route::post('/products', 'ProductController@store')->name('products.store');
    Route::put('/products', 'ProductController@update')->name('products.update');
    Route::put('/delete-product', 'ProductController@delete')->name('products.delete');
    Route::put('/products-active', 'ProductController@activeProducts')->name('products.active');

    /*
    |--------------------------------------------------------------------------
    | Rutas de ordenes de fonditas
    |--------------------------------------------------------------------------
    */
    Route::post('/order', 'OrderController@store')->name('orders.store');

    /*
    |--------------------------------------------------------------------------
    | Rutas de ordenes de usuario
    |--------------------------------------------------------------------------
    */
    Route::get('/my-orders', 'UserController@myOrders')->name('user.orders');
    Route::get('/my-favourites', 'UserController@myFavourites')->name('user.favourites');
    Route::get('/my-restaurant', 'UserController@myRestaurant')->name('user.restaurant');
    Route::put('/my-restaurant-open-close', 'UserController@openCloseRestaurant')->name('user.restaurant.open.close');
    Route::post('/favourite', 'UserController@addFavourite')->name('favourite.store');
    Route::post('/favourite-delete', 'UserController@removeFavourite')->name('favourite.delete');

});
